openapi: 3.0.0
info:
  title: DCTx FLAG API
  description: >-
    API for FLAG Web Services
  version: "1.0.0"
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
tags:
  - name: donation
    description: Endpoints for donation
  - name: static
    description: Endpoints for static values to be used in the forms
  - name: dashboard
    description: Endpoints for administrators
  - name: public
    description: Publicly accessible endpoints
security:
  - BearerAuth: []
paths:
  /auth/login:
    post:
      deprecated: true
      tags:
        - public
      summary: >-
        Authentication endpoint. This requires a username and password/pin. This
        will return an authentication token required for accessing the other
        endpoints.
      operationId: auth
      requestBody:
        description: >-
          Login request body contains the username/mobile-number and password/pin
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/LoginRequest'
      responses:
        200:
          description: >-
            A successful login will return an access code which is a bearer token
            needed to access the other endpoints.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/LoginResponse'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /auth/client/token:
    post:
      tags:
        - public
      summary: >-
        Authentication endpoint. This requires a client ID and secret. This
        will return an access token required for accessing the other
        endpoints.
      operationId: authClientToken
      requestBody:
        description: >-
          Login request body contains the client ID and client secret
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ClientTokenRequest'
      responses:
        200:
          description: >-
            A successful login will return an access token which is a bearer token
            needed to access the other endpoints.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ClientTokenResponse'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /sms/message:
    post:
      tags:
        - requestor
        - dashboard
      summary: >-
        Endpoint to send an SMS message. This requires a mobile number and a message. This
        will return the message reference ID and the status.
      operationId: sendSmsMessage
      requestBody:
        description: >-
          SMS message request body contains the mobile number and a short message (160 ASCII characters)
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/SendSmsMessageRequest'
      responses:
        200:
          description: >-
            A successful request will return the message status (Pending, Sent, or Failed) and the message reference ID,
            which can used to check if the status has been updated.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SendSmsMessageResponse'
        default:
          description: >-
            An error occured while performing the operation. A detailed JSON error will be returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /sms/message/{id}/status:
    get:
      tags:
        - requestor
        - dashboard
      summary: >-
        Retrieve the status of a message
      operationId: getMessageStatus
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            description: the ID of the message
            example: '80404765'
      responses:
        200:
          description: >-
            The status of the message
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/SendSmsMessageResponse'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donors:
    get:
      tags:
        - donation
      summary: >-
        Retrieve the details of a donor
      operationId: getDonors
      parameters:
      responses:
        200:
          description: >-
            The list of all donors
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donor'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    post:
      tags:
        - donation
      summary: Creates a donor.
      operationId: createDonor
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Donor'
      responses:
        200:
          description: >-
            Donor created
          content:
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
                
  /donors/{id}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve the details of a donor by id
      operationId: getDonorById
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: string
            description: the ID of the donor
            example: '1'
      responses:
        200:
          description: >-
            The details of the donor
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donor'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donors/mobile/{number}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve the details of a donor by mobile number
      operationId: getDonorByMobile
      parameters:
        - name: number
          in: path
          required: true
          schema:
            type: string
            description: the mobile number of the donor
            example: '630000000000'
      responses:
        200:
          description: >-
            The details of the donor
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donor'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donors/sector/{id}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve the details of a donor by sector
      operationId: getDonorBySector
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: number
            description: Sector classification of the donor
            example: 1
      responses:
        200:
          description: >-
            The details of the donor
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donor'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donations:
    get:
      tags:
        - donation
      summary: >-
        Retrieve all donations
      operationId: getDonations
      parameters:
      responses:
        200:
          description: >-
            The details of the donations
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donation'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - donation
      summary: Creates a donation.
      operationId: createDonation
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Donation'
      responses:
        200:
          description: >-
            The details of the donation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donation'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    delete:
      tags:
        - donation
      summary: Deletes all donations.
      operationId: deleteDonations
      responses:
        200:
          description: >-
            Package deleted
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
           
  /donations/{id}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve donation by Id
      operationId: getDonationById
      parameters:
          - name: id
            in: path
            required: true
            type: number
            description: Id of the donation
            example: 1
      responses:
        200:
          description: >-
            The details of the donation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donation'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donations/request/{id}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve donation by request Id
      operationId: getDonationByRequestId
      parameters:
          - name: id
            in: path
            required: true
            type: number
            description: Id of the donation request
            example: 1
      responses:
        200:
          description: >-
            The details of the donation
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Donation'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

    delete:
      tags:
        - donation
      summary: >-
        Delete donation by Id
      operationId: getDonationById
      parameters:
          - name: id
            in: path
            required: true
            type: number
            description: Id of the donation
            example: 1
      responses:
        200:
          description: >-
            Donation deleted
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donationrequests:
    get:
      tags:
        - donation
      summary: >-
        Retrieve all donation requests
      operationId: getDonationRequests
      parameters:
      responses:
        200:
          description: >-
            The details of the requests
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DonationRequest'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
    post:
      tags:
        - donation
      summary: Creates a donation request.
      operationId: createDonationRequests
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/DonationRequest'
      responses:
        200:
          description: >-
            The details of the packages
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DonationRequest'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donationrequests/{id}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve donation request by Id
      operationId: getDonationRequestByid
      parameters:
          - name: id
            in: path
            required: true
            type: number
            description: Id of the donation request
            example: 1
      responses:
        200:
          description: >-
            The details of the requests
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DonationRequest'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /donationrequests/donor/{id}:
    get:
      tags:
        - donation
      summary: >-
        Retrieve all donation request of a donor
      operationId: getDonationRequestByDonorid
      parameters:
          - name: id
            in: path
            required: true
            type: number
            description: Id of the donor
            example: 1
      responses:
        200:
          description: >-
            The details of the requests
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DonationRequest'
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/currencies:
    get:
      tags:
        - static
      summary: >-
        Retrieve all currencies
      operationId: getCurrencies
      responses:
        200:
          description: >-
            All currencies
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/donationtypes:
    get:
      tags:
        - static
      summary: >-
        Retrieve all types of donations
      operationId: getDonationTypes
      responses:
        200:
          description: >-
            All donation types
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

                
  /static/donorsectors:
    get:
      tags:
        - static
      summary: >-
        Retrieve all types of donor sectors
      operationId: getDonorSectors
      responses:
        200:
          description: >-
            All donor sectors
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/units:
    get:
      tags:
        - static
      summary: >-
        Retrieve all units
      operationId: getUnits
      responses:
        200:
          description: >-
            All units
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/servicetypes:
    get:
      tags:
        - static
      summary: >-
        Retrieve all service types
      operationId: getServiceType
      responses:
        200:
          description: >-
            All service types
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/beneficiarysectors:
    get:
      tags:
        - static
      summary: >-
        Retrieve all beneficiary sectors
      operationId: getBenefeciarySectors
      responses:
        200:
          description: >-
            All beneficiary sectors
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/requesttypes:
    get:
      tags:
        - static
      summary: >-
        Retrieve all request types
      operationId: getRequestTypes
      responses:
        200:
          description: >-
            All request types
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/items:
    get:
      tags:
        - static
      summary: >-
        Retrieve all donation items
      operationId: getDonationItems
      responses:
        200:
          description: >-
            All donation items
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

                
  /static/regions:
    get:
      tags:
        - static
      summary: >-
        Retrieve all regoins
      operationId: getRegions
      responses:
        200:
          description: >-
            All regions
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'

  /static/provinces:
    get:
      tags:
        - static
      summary: >-
        Retrieve all provinces
      operationId: getProvinces
      responses:
        200:
          description: >-
            All regions
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
                
  /static/countries:
    get:
      tags:
        - static
      summary: >-
        Retrieve all countries
      operationId: getCountries
      responses:
        200:
          description: >-
            All regions
        default:
          description: >-
            An error occured while performing the operation. A details JSON error is returned.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Error'
                
components:
  securitySchemes:
    BearerAuth:
      type: http
      scheme: bearer

  schemas:
    Error:
      type: object
      properties:
        id:
          type: string
          description: >-
            a unique identifier for this particular occurrence of the problem.
        links:
          type: object
          description: >-
            a links object containing the following members;
          properties:
            about:
              type: string
              description: >-
                a link that leads to further details about this particular occurrence of the problem.
          additionalProperties: true
        status:
          type: string
          description: >-
            the HTTP status code applicable to this problem, expressed as a
            string value.
        code:
          type: string
          description: >-
            an application-specific error code, expressed as a string value.
        title:
          type: string
          description: >-
            a short, human-readable summary of the problem that SHOULD NOT
            change from occurrence to occurrence of the problem, except for
            purposes of localization.
        detail:
          type: string
          description: >-
            a human-readable explanation specific to this occurrence of the
            problem. Like title, this field’s value can be localized.
        source:
          type: array
          description: >-
            an object containing references to the source of the error, optionally including any of the following members;
          items:
            type: object
            properties:
              pointer:
                type: string
                description: >-
                  a JSON Pointer [RFC6901] to the associated entity in the
                  request document [e.g. "/data" for a primary data object, or "
                  /data/attributes/title" for a specific attribute].
              parameter:
                type: string
                description: >-
                  a string indicating which URI query parameter caused the error
                  .
        meta:
          type: object
          description: >-
            a meta object containing non-standard meta-information about the error.
          additionalProperties: true
    LoginRequest:
      type: object
      properties:
        username:
          type: string
          description: >-
            The username to login. This could be the mobile number for the
            requestor clients
          example: '+630000000000'
        password:
          type: string
          format: password
          description: >-
            The password/pin needed to login.
          example: '123456'
    LoginResponse:
      type: object
      properties:
        access-code:
          type: string
          description: >-
            A bearer token needed to access the protected endpoints
        refresh-code:
          type: string
          description: >-
            A token required to refresh an access token
    ClientTokenRequest:
      type: object
      properties:
        client_id:
          type: string
          description: >-
            The client ID for the apps using the API. This should be requested from the Keycloak admin.
          example: 'frontend-dev'
        client_secret:
          type: string
          description: >-
            The secret for the apps using the API. This should be requested from the Keycloak admin.
          example: '776aabe1-a635-4b36-b635-06b28d778268'
    ClientTokenResponse:
      type: object
      properties:
        access_token:
          type: string
          description: >-
            A bearer token needed to access the protected endpoints
          example: 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lk...'

    Requestor:
      type: object
      properties:
        id:
          type: integer
          description: >-
            The ID of the requestor
          example: 1
        first_name:
          type: string
          description: >-
            The requestor's first name
        last_name:
          type: string
          description: >-
            The requestor's last name
        mobile_number:
          type: string
          description: >-
            The requestor's mobile number
          example: '+639990001111'
        type:
          type: string
          description: >-
            The requestor type
          enum:
            - INDIVIDUAL
            - ORGANIZATION
          example: INDIVIDUAL
        group:
          type: string
          description: >-
            The requestor's group should it be an organization
          example: ACME Corp.
        email:
          type: string
          description: >-
            The requestor's email address
          format: email
          example: requestor@acme.corp
        status:
          type: string
          description: >-
            The status of the requestor
          enum:
            - GUEST
            - PENDING
            - ACTIVE
            - INACTIVE
          example: ACTIVE
    SendSmsMessageRequest:
      type: object
      properties:
        number:
          type: string
          description: >-
            The mobile number to send the SMS message.
          example: '+630000000000'
        message:
          type: string
          description: >-
            The short message (max 160 ASCII characters) to send.
          example: 'This is the message.'
    SendSmsMessageResponse:
      type: object
      properties:
        message_id:
          type: string
          description: >-
            The reference ID for the message that can be used to check its updated status later.
        status:
          type: string
          description: >-
            The status of the message (Pending, Sent, or Failed)

    Donor:
      type: object
      properties:
        id:
          type: integer
          description: >-
            The ID of the donor
          example: 1
        contact_person:
          type: string
          description: >-
            The donor's name or representative of an affiliation or organization
        affiliation_org:
          type: string
          description: >-
            The name organization or affiliation giving the donation
        mobile_number:
          type: string
          description: >-
            The donor's mobile number
          example: '+639990001111'
        landline_number:
          type: string
          description: >-
            The donor's landline number
          example: '+639990001111'
        isAnonymous:
          type: boolean
          description: >-
            Indicate if donor wanted to be anonymous
          example: 'false'
        isInternational:
          type: boolean
          description: >-
            Indicate if donor is from outside the country
          example: 'false'
        sector_type:
          type: string
          description: >-
            The donor type
          enum:
            - GOV
            - LGU
            - PRINGO
            - INGO
          example: GOV
        email_address:
          type: string
          description: >-
            The donor's email address
          format: email
          example: donor@acme.corp
        address:
          type: object
          description: >-
            This contains the street no., block, house number etc.
          example: {
                    "address_line_one": "Block1, AFPOVAI",
                    "barangay": "Western Bicutan",
                    "city": "Taguig",
                    "province": "Metro Manila",
                    "region": "Region VII",
                    "country": "Philippines"
                  }

    Donation:
      type: object
      properties:
        id:
          type: integer
          description: >-
            The ID of the donation
          example: 1
        currency:
          type: string
          description: >-
            The currency used for the donation
          example: PHP
        value:
          type: string
          description: >-
            The total value of the donation
          example: '1000'
        remarks:
          type: string
          description: >-
            The comments about a donation
          example: nice
        photo_reference:
          type: string
          description: >-
            The photo of the donation
          example: "lucky me.jpg,alcohol.jpg"
        file_reference:
          type: string
          description: >-
            The photo of the donation
          example: "MOA.pdf"
        donation_date:
          type: string
          description: >-
            The date of the donation
          example: "2020-05-30 09:00:00"
        donation_items:
          type: array
          items:
              type: object
          description: >-
            The contents inside a donation
          example: [{
                      "donation_type": "MDS",
                      "weight": 50,
                      "total_unit_value": 10,
                      "unit": "BX",
                      "estimated_cost": 5000,
                      "currency": "PHP",
                      "description": "nice"
                    }]
        cash_donations:  
          type: array
          items:
              type: object
          description: >-
            The cash donations
          example: [{
                      "amount": 1000,
                      "currency": "PHP",
                      "description": "alay para sa buhay"
                   }]
        services: 
          type: array
          items:
              type: object
          description: >-
              The services offered
          example: [{
                     "service_type": "SV001",
                      "currency": "PHP",
                      "estimated_cost": 5000,
                      "hours": 100,
                      "description": "doctors",
                      "start_date": "2020-05-30 00:00:00",
                      "end_date": "2020-07-30 00:00:00"
                    }]
        rentals:
          type: array
          items:
              type: object
          description: >-
              The rentals offered
          example:  [{
                      "currency": "PHP",
                      "estimated_cost": 10000,
                      "description": "dorms for doctors",
                      "start_date": "2020-05-30 00:00:00",
                      "end_date": "2020-07-30 00:00:00"
                    }]


    DonationRequest:
      type: object
      properties:
        id:
          type: integer
          description: >-
            The ID of the donation request
          example: 1
        request_type:
          type: string
          description: >-
            The type of donation request
          example: "FOR_REPORTING_ONLY"
        donor:
          type: object
          description: >-
            The one who makes the donation
          example: {
              "contact_person": "dctx",
              "affiliation_org": "vlad",
              "mobile_number": "12345",
              "landline_number": "12345",
              "email_address": "donor@dctx.com",
              "sector_type": "GOV",
              "isInternational":false,
              "isAnonymous":false,
              "address":{
                  "address_line_one":  "Block1, AFPOVAI",
                  "barangay": "Western Bicutan",
                  "city": "Taguig",
                  "province": "Metro Manila",
                  "region":"Region VII",
                  "country" : "Philippines"
              }
            }
        beneficiary:
          type: object
          description: >-
            The one who receives the donation
          example: {
              "contact_person": "dctx",
              "affiliation_org": "vlad",
              "mobile_number": "12345",
              "landline_number": "12345",
              "email_address": "donor@dctx.com",
              "sector_type": "LGU",
              "address":{
                  "address_line_one":  "Jingoy Block",
                  "barangay": "Barangay 101",
                  "city": "San Juan",
                  "province": "Metro Manila",
                  "region":"Region VII",
                  "country" : "Philippines"
              }
            }

        donations:
          type: object
          description: >-
            The donation
          example: [{ "photo_reference": "medicines.jpg",
                      "file_reference": "MOA.pdf",
                      "donation_date": "2020-05-30 09:00:00",
                      "remarks":"nice",
                      "donation_items":[{
                              "donation_type": "MDS",
                              "weight": 50,
                              "dimension":"2x2x2",
                              "total_unit_value": 10,
                              "unit": "BX",
                              "estimated_cost": 5000,
                              "currency": "PHP",
                              "description": "nice"
                            },
                            {
                              "donation_type": "MDS",
                              "weight": 50,
                              "dimension":"2x2x2",
                              "total_unit_value": 10,
                              "unit": "BX",
                              "estimated_cost": 5000,
                              "currency": "PHP",
                              "description": "nice"    
                            }],
                      "cash_donations":[{
                              "amount": 1000,
                              "currency": "PHP",
                              "description": "alay para sa buhay"
                        }],   
                      "services": [{
                              "service_type": "SV001",
                              "currency": "PHP",
                              "estimated_cost": 5000,
                              "hours": 100,
                              "description": "doctors",
                              "start_date": "2020-05-30 00:00:00",
                              "end_date": "2020-07-30 00:00:00"
                      }],
                      "rentals": [{
                              "currency": "PHP",
                              "estimated_cost": 10000,
                              "description": "dorms for doctors",
                              "start_date": "2020-05-30 00:00:00",
                              "end_date": "2020-07-30 00:00:00"
                      }]
                    }]



          
    
          


        
     
