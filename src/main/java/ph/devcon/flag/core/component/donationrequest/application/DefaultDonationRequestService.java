package ph.devcon.flag.core.component.donationrequest.application;

import lombok.extern.slf4j.Slf4j;
import ph.devcon.flag.core.component.beneficiary.application.BeneficiaryService;
import ph.devcon.flag.core.component.beneficiary.domain.Beneficiary;
import ph.devcon.flag.core.component.donationrequest.domain.*;
import ph.devcon.flag.core.component.donations.application.*;
import ph.devcon.flag.core.component.donor.application.*;
import ph.devcon.flag.core.component.donor.domain.*;
import ph.devcon.flag.core.component.exception.InvalidRequestException;
import ph.devcon.flag.core.component.utils.helper.Mappers.*;
import ph.devcon.flag.core.port.persistence.*;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import static java.lang.String.format;

import java.time.LocalDateTime;

@Slf4j
@ApplicationScoped
public class DefaultDonationRequestService implements DonationRequestService{
    
    @Inject
    DonationRequestRepository donationRequestRepository;

    @Inject
    RequestTypeRepository requestTypeRepository;

    @Inject
    DonorSectorRepository sectorRepository;

    @Inject
    DonorService donorService;

    @Inject
    BeneficiaryService beneficiaryService;

    @Inject
    DonationService packageService;

    @Override
    public DonationRequestDTO getDonationRequestById(Long donationRequestId) {
        return toDonationRequestDTO(donationRequestRepository.findDonationRequest(donationRequestId));
    }

    @Override
    public List<DonationRequestDTO> getDonationRequestByDonorId(Long donorId) {
        List<DonationRequest> requests = donationRequestRepository.findDonationRequestByDonorId(donorId);
        return requests.stream()
                .map(x-> toDonationRequestDTO(x))
                .collect(Collectors.toList());
    }

    @Override
    public RequestType getRequestTypeByName(String requestType) {
        return requestTypeRepository.findByName(requestType);
    }

    @Override
    @Transactional
    public void createUpdateDonationRequest(DonationRequestDTO donationRequestDTO) throws Exception {
        DonationRequest donationRequest = newDonationRequest(donationRequestDTO);
        donationRequest = donationRequestRepository.createUpdateDonationRequest(donationRequest);
        log.info("Donation Request ID: " + donationRequest.getId());

        for(DonationDTO donation: donationRequestDTO.getDonations()){
            packageService.createUpdateDonation(donation, donationRequest);   
        }
    }

    private DonationRequest newDonationRequest(DonationRequestDTO donationRequestDTO) throws Exception {  
        RequestType requestType = requestTypeRepository.findByName(donationRequestDTO.getRequestType());

        if(requestType == null)
            throw new InvalidRequestException(format("RequestType %s not found.", donationRequestDTO.getRequestType()));

   
        DonationRequest donationRequest = new DonationRequest();
        donationRequest.setRequestType(requestType);
        donationRequest.setCreatedDate(LocalDateTime.now());
        donationRequest.setDateLastModified(LocalDateTime.now());

        Donor donor = donorService.createUpdateDonorInternal(donationRequestDTO.getDonorDTO());
        donationRequest.setDonor(donor);

        Beneficiary beneficiary = beneficiaryService.createUpdateBeneficiaryInternal(donationRequestDTO.getBeneficiaryDTO());
        donationRequest.setBeneficiary(beneficiary);

        return donationRequest;
    }
  
    @Override
    public List<DonationRequestDTO> getDonationRequests() {
       List<DonationRequest> requests = donationRequestRepository.getAllDonationRequests();
       return requests.stream()
             .map(x-> toDonationRequestDTO(x))
             .collect(Collectors.toList());
    }

    public DonationRequestDTO toDonationRequestDTO(DonationRequest request){
        DonationRequestDTO dto = DonationRequestDTO.builder()
                                 .id(request.getId())
                                 .donorDTO(DonorMapper.toDonorDTO(request.getDonor()))
                                 .beneficiaryDTO(BeneficiaryMapper.toBeneficiaryDTO(request.getBeneficiary()))
                                 .donations(DonationMapper.toDonationsDTO(request.getDonations()))
                                 .requestType(request.getRequestType().getName())
                                 .lastModified(request.getDateLastModified())
                                 .createdDate(request.getCreatedDate())
                                 .build();

        return dto;
    }
}
