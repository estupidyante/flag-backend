package ph.devcon.flag.core.port.persistence;

import ph.devcon.flag.core.component.donationrequest.domain.DonationRequest;

import java.util.List;

public interface DonationRequestRepository {
    DonationRequest createUpdateDonationRequest(DonationRequest donationRequest);
    DonationRequest findDonationRequest(Long donationRequestId);
    List<DonationRequest> findDonationRequestByDonorId(Long donorId);
    List<DonationRequest> getAllDonationRequests();
}
