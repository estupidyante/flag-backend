package ph.devcon.flag.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ph.devcon.flag.core.component.donationrequest.domain.DonationRequest;
import ph.devcon.flag.core.port.persistence.DonationRequestRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.List;

@ApplicationScoped
public class PanacheDonationRequestRepository implements DonationRequestRepository, PanacheRepository<DonationRequest> {
    @Inject
    protected EntityManager em;

    @Override
    public DonationRequest createUpdateDonationRequest(DonationRequest donationRequest) {
        if(donationRequest.getId() != 0)
            em.merge(donationRequest);
        else
            persist(donationRequest);

        flush();
        return donationRequest;
    }

    @Override
    public DonationRequest findDonationRequest(Long donationRequestId) {
        return find("id", donationRequestId).firstResult();
    }

    @Override
    public List<DonationRequest> findDonationRequestByDonorId(Long donorId) {
        return find("donor_id", donorId).list();
    }

    @Override
    public List<DonationRequest> getAllDonationRequests() {
        return listAll();
    }
}
