package ph.devcon.flag.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ph.devcon.flag.core.component.utils.domain.Province;
import ph.devcon.flag.core.port.persistence.ProvinceRepository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PanacheProvinceRepository implements ProvinceRepository, PanacheRepository<Province> {

    @Override
    public List<Province> findAllProvinces() {
        return listAll();
    }

    @Override
    public Province findByRegion(String region) {
        return find("region", region).firstResult();
    }

    @Override
    public Province findByName(String name) {
        return find("name", name).firstResult();
    }

    @Override
    public Province findById(int id) {
        return find("id", id).firstResult();
    }
}
