package ph.devcon.flag.infrastructure.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import ph.devcon.flag.core.component.donor.domain.Donor;
import ph.devcon.flag.core.port.persistence.DonorRepository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

@ApplicationScoped
public class PanacheDonorRepository implements DonorRepository, PanacheRepository<Donor> {

    @Inject
    protected EntityManager em;
    
    @Override
    public List<Donor> findAllDonors() {
        return listAll();
    }

    @Override
    public Donor findById(final long id) {
        return find("id", id).firstResult();
    }

    @Override
    public Donor findByMobile(final String mobile) {
        return find("mobile_number", mobile).firstResult();
    }

    @Override
    public List<Donor> findBySector(int sectorTypeId) {
        return list("sector_type_id", sectorTypeId);
    }

    @Override
    public Donor createUpdateDonor(Donor donor) {
        
        if(donor.getId() != 0)
            em.merge(donor);
        else
            persist(donor);
            
        flush();
        return donor;
    }
}
